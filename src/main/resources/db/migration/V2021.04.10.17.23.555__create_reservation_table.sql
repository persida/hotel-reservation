CREATE TABLE public.reservation
(
    id integer,
    guest_id integer not null,
    hotel_id integer not null,
    room_id integer not null,
    start_date date not null,
    end_date date not null,
    number_guests integer not null,
    status character varying(30) not null,
    CONSTRAINT reservation_pkey PRIMARY KEY (id)
)