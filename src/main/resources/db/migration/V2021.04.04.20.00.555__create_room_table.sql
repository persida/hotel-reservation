CREATE TABLE public.room
(
    id integer,
    room_number character varying(4),
    price numeric(7,2),
    number_persons integer,
    type character varying(20),
    hotel_id integer,
    CONSTRAINT room_pkey PRIMARY KEY (id),
    CONSTRAINT fk_room_hotel_id FOREIGN KEY (hotel_id)
        REFERENCES public.hotel (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)