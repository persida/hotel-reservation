CREATE TABLE public.hotel
(
    id SERIAL NOT NULL,
    name character varying(50),
    location character varying(50),
    CONSTRAINT hotel_pkey PRIMARY KEY (id)
)