package com.persidacvetkoska.hotelreservationsystemjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotelReservationSystemJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotelReservationSystemJpaApplication.class, args);
	}

}
