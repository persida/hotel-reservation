package com.persidacvetkoska.hotelreservationsystemjpa.util;

import com.persidacvetkoska.hotelreservationsystemjpa.reservation.ReservationStatusEnum;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(FIELD)
@Constraint(validatedBy = ValueOfEnumValidator.class)
@Retention(RUNTIME)
@Documented
public @interface ValueOfEnum {
    ReservationStatusEnum[] anyOf();
    Class<? extends Enum<?>> enumClass();
    String message() default "must be any of {anyOf}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
