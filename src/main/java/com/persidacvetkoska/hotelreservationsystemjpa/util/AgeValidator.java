package com.persidacvetkoska.hotelreservationsystemjpa.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class AgeValidator implements ConstraintValidator<ValidAge, LocalDate> {

    @Override
    public boolean isValid(LocalDate value, ConstraintValidatorContext context){
        return value != null && (LocalDate.from(value).until(LocalDate.now(), ChronoUnit.YEARS) >= 18);
    }

    @Override
    public void initialize(ValidAge parameter) {

    }

}

