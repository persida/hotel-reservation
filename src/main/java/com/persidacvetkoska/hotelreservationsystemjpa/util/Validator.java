package com.persidacvetkoska.hotelreservationsystemjpa.util;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.NumberOfGuestsExceededException;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceValidationException;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.RoomAlreadyReservedException;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.Room;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.RoomRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.reservation.Reservation;
import com.persidacvetkoska.hotelreservationsystemjpa.reservation.ReservationRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.reservation.ReservationUpdateRequest;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class Validator {
    public void checkNumberOfGuests(Room room, Integer numberOfGuests){
        if (room.numberOfPersons < numberOfGuests){
            throw new NumberOfGuestsExceededException("The number of guests exceeds the number of beds in the room!");
        }
    }
    public void checkRoomAvailability(Room room, LocalDate startDate, LocalDate endDate, ReservationRepository reservationRepository){
        var foundReservation =  reservationRepository.findFirstReservationByRoomAndRange(room.id, startDate, endDate);
        if (foundReservation.isPresent()){
            throw new RoomAlreadyReservedException("Room not available for provided period!");
        }
    }
    public void processRoomAvailability(Reservation reservation, ReservationUpdateRequest request, ReservationRepository reservationRepository){
        LocalDate newEndDate = request.startDate.plusDays(request.numberOfDays);
        if(!reservation.startDate.equals(request.startDate) || !reservation.endDate.equals(newEndDate)){

            var foundReservation =  reservationRepository.findReservationByRoomAndRangeUpdate(
                    reservation.id, reservation.room.id, request.startDate, newEndDate);

            if (foundReservation.isPresent()){
                throw new RoomAlreadyReservedException("Room not available for provided period!");
            }
        }
    }
    public void checkRoomNumberAlreadyExists(Integer hotelId, String roomNumber, RoomRepository roomRepository){
        var room = roomRepository.findByRoomNumberAndHotelId(roomNumber, hotelId);
        if (room.isPresent()){
            throw new ResourceValidationException("Room with room number " + roomNumber + " already exists!");
        }

    }
}
