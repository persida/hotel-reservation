package com.persidacvetkoska.hotelreservationsystemjpa.util;

public class ReturnValueDto {
    public String value;

    public ReturnValueDto(String value) {
        this.value = value;
    }
}
