package com.persidacvetkoska.hotelreservationsystemjpa.hotel;

import com.persidacvetkoska.hotelreservationsystemjpa.util.ReturnValueDto;
import com.persidacvetkoska.hotelreservationsystemjpa.util.VoidResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/hotels")
@RequiredArgsConstructor
public class HotelResource {

    private final HotelService service;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDto create(@RequestBody HotelRequest request){

        return service.create(request);
    }

    @PutMapping(path = "/{id}", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDto update(@PathVariable Integer id, @RequestBody HotelRequest request){
        return service.update(id, request);
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public HotelDto findHotelById(@PathVariable Integer id){
        return service.findHotelById(id);
    }

    @GetMapping(path="/find-all", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<HotelDto> findPage(Pageable pageable){
        return service.findPage(pageable);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<HotelDto> findPageByNameOrLocation(@RequestParam(required = false) String name, @RequestParam(required = false) String location, Pageable pageable){
        return service.findPageByNameAndLocation(new HotelSearchRequest(name, location, pageable));
    }

    @GetMapping(path = "/{id}/get-number-guests", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReturnValueDto getNumberGuests(@PathVariable Integer id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate){
        return service.getNumberGuests(id, onDate);
    }

    @GetMapping(path = "/{id}/get-number-reserved-rooms", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReturnValueDto getNumberReservedRooms(@PathVariable Integer id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate){
        return service.getNumberReservedRooms(id, onDate);
    }

    @GetMapping(path = "/{id}/get-number-free-rooms", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReturnValueDto getNumberFreeRooms(@PathVariable Integer id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate){
        return service.getNumberFreeRooms(id, onDate);
    }


    @GetMapping(path = "/{id}/get-profit", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public ReturnValueDto getProfitForPeriod(
            @PathVariable Integer id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate){
        return service.getProfitForPeriod(id, onDate);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<VoidResponse> deleteHotelById(@PathVariable Integer id){
        service.deleteHotelById(id);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidResponse(true));
    }
}
