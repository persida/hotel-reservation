package com.persidacvetkoska.hotelreservationsystemjpa.hotel;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class HotelSearchRequest {
    public String name;
    public String location;
    public Pageable pageable;

    public HotelSearchRequest(String name, String location, Pageable pageable) {
        this.name = name;
        this.location = location;
        this.pageable = pageable;
    }
    public Specification<Hotel> generateSpecification(){
        Specification<Hotel> specification = Specification.where(null);
        if (StringUtils.hasText(name)){
            specification = specification.and(HotelSpecifications.byNameLiteralEquals(name));
        }
        if (StringUtils.hasText(location)){
            specification = specification.and(HotelSpecifications.byLocationLiteralEquals(location));
        }
        return specification;
    }
}
