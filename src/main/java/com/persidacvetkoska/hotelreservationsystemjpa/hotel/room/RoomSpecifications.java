package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import org.springframework.data.jpa.domain.Specification;

public class RoomSpecifications {

    private RoomSpecifications(){}

    public static Specification<Room> byTypeLiteralEquals(final String type){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("type")),
                type.toLowerCase()));
    }
}
