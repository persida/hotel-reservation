package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;


import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

public class RoomSearchRequest {
    public Integer hotelId;
    public String type;
    public Pageable pageable;

    public RoomSearchRequest(Integer hotelId, String type, Pageable pageable) {
        this.hotelId = hotelId;
        this.type = type;
        this.pageable = pageable;
    }

    public Specification<Room> generateSpecification(){
        Specification<Room> specification = Specification.where(null);
        if (StringUtils.hasText(type)){
            specification = specification.and(RoomSpecifications.byTypeLiteralEquals(type));
        }
        return specification;
    }
}
