package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceValidationException;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.HotelRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.reservation.ReservationRepository;
import com.persidacvetkoska.hotelreservationsystemjpa.util.Validator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class RoomService {

    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;
    private final ReservationRepository reservationRepository;
    private final Validator validator;

    public RoomDto create(Integer hotelId, RoomRequest request){
        var hotel = hotelRepository.findById(hotelId)
                        .orElseThrow(() -> new ResourceNotFoundException("Hotel with id= " + hotelId + " doesn't exist!"));

        validator.checkRoomNumberAlreadyExists(hotelId, request.roomNumber, roomRepository);
        var room = new Room(request.roomNumber, request.type.type, request.type.numberOfPersons, request.type.price, hotel);

        return roomRepository.save(room).toDto();
    }

    public RoomDto update(Integer hotelId, Integer roomId, RoomRequest request){

        var currentRoom = roomRepository.findByIdAndHotelId(hotelId, roomId)
                .orElseThrow(() -> new ResourceNotFoundException("Room doesn't exist!"));
        var foundRoomNumber = roomRepository.findByRoomNumberAndHotelId(request.roomNumber, hotelId);
        if (foundRoomNumber.isPresent() && foundRoomNumber.get().id != roomId) {
            throw new ResourceValidationException("Room number already exists");
        }
        currentRoom.roomNumber = request.roomNumber;
        currentRoom.type = request.type.type;
        currentRoom.numberOfPersons = request.type.numberOfPersons;
        currentRoom.price = request.type.price;
        var updatedRoom = roomRepository.save(currentRoom);
        return updatedRoom.toDto();
    }

    public RoomDto findRoomById(Integer hotelId, Integer roomId){
        var foundRoom = roomRepository.findByIdAndHotelId(hotelId,roomId)
                        .orElseThrow(() -> new ResourceNotFoundException("Room not found!"));
        return foundRoom.toDto();
    }

    public Page<RoomDto> findPage(Integer hotelId, Pageable pageable){
        var foundRooms = roomRepository.findByHotelId(hotelId, pageable);
        if (foundRooms.isEmpty()){
            throw new ResourceNotFoundException("No rooms found");
        }
        return foundRooms.map(room -> room.toDto());

    }

    public Page<RoomDto> findPageByType(RoomSearchRequest request){
        var rooms = roomRepository.findAll(request.generateSpecification(), request.pageable);
        return rooms.map(room -> room.toDto());
    }

    public Page<RoomDto> findReservedPage(Integer hotelId, LocalDate onDate, Pageable pageable){
        var foundRooms = roomRepository.findReservedForDate(hotelId, onDate, pageable);
        return foundRooms.map(room -> room.toDto());
    }

    public Page<RoomDto> findFreePage(Integer hotelId, LocalDate onDate, Pageable pageable){
        var foundRooms = roomRepository.findFreeForDate(hotelId, onDate, pageable);
        return  foundRooms.map(room -> room.toDto());
    }

    public void delete(Integer hotelId, Integer roomId){
        var foundRoom = roomRepository.findByIdAndHotelId(roomId, hotelId)
                        .orElseThrow(() -> new ResourceNotFoundException("Room with room id = " + roomId + " not found!"));
        var foundReservation = reservationRepository.findFirstByRoomId(foundRoom.id);
        if (foundReservation.isPresent()){
            throw new ResourceValidationException("Room can't be deleted, there are reservations for it!");
        }
        roomRepository.delete(foundRoom);
    }


}
