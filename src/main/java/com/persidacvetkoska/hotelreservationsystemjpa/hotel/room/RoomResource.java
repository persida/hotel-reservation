package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.persidacvetkoska.hotelreservationsystemjpa.util.VoidResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/hotels/{hotelId}/rooms")
@RequiredArgsConstructor
public class RoomResource {

    private final RoomService service;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public RoomDto create(@PathVariable final Integer hotelId, @RequestBody final RoomRequest request){
        return service.create(hotelId, request);
    }

    @PutMapping(path = "/{roomId}", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public RoomDto update(@PathVariable final Integer hotelId, @PathVariable final Integer roomId, @RequestBody final RoomRequest request){
        return service.update(hotelId, roomId, request);
    }

    @GetMapping(path = "/{roomId}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public RoomDto findRoomById(@PathVariable Integer hotelId, @PathVariable Integer roomId){
        return service.findRoomById(hotelId, roomId);
    }

    @GetMapping(path = "/find-all", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<RoomDto> findPage(@PathVariable Integer hotelId, Pageable pageable){
        return service.findPage(hotelId, pageable);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<RoomDto> findPageByType(@PathVariable Integer hotelId, @RequestParam(required = false) String type, Pageable pageable){
        return service.findPageByType(new RoomSearchRequest(hotelId, type, pageable));
    }

    @GetMapping(path = "/find-reserved", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<RoomDto> findReservedPage(@PathVariable Integer hotelId, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate, Pageable pageable){
        return service.findReservedPage(hotelId, onDate, pageable);
    }

    @GetMapping(path = "/find-free", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<RoomDto> findFreePage(@PathVariable Integer hotelId, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate onDate, Pageable pageable){
        return service.findFreePage(hotelId, onDate, pageable);
    }

    @DeleteMapping(path = "/{roomId}")
    public ResponseEntity<VoidResponse> delete(@PathVariable Integer hotelId, @PathVariable Integer roomId){
        service.delete(hotelId, roomId);
        return ResponseEntity.status(HttpStatus.OK).body(new VoidResponse(true));
    }
}
