package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import java.math.BigDecimal;

public class RoomDto {
    public Integer id;
    public String roomNumber;
    public String type;
    public Integer numberOfPersons;
    public BigDecimal price;
    public Integer hotelId;

    public RoomDto(Integer id, String roomNumber, String type, Integer numberOfPersons, BigDecimal price, Integer hotelId) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.type = type;
        this.numberOfPersons = numberOfPersons;
        this.price = price;
        this.hotelId = hotelId;
    }
}
