package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.RoomTypeNotValidException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public enum RoomType {
    SINGLE("Single room", 1, BigDecimal.valueOf(30)),
    DOUBLE("Double room", 2, BigDecimal.valueOf(40)),
    KING("King room", 2, BigDecimal.valueOf(100)),
    FAMILY("Family room", 6, BigDecimal.valueOf(200)),
    KING_SUITE("King suite", 4, BigDecimal.valueOf(1500));

    public final String type;
    public final int numberOfPersons;
    public final BigDecimal price;

    private RoomType(String type, int numberOfPersons, BigDecimal price ){
        this.type = type;
        this.numberOfPersons = numberOfPersons;
        this.price = price;
    }

    private static final Map<String, RoomType> BY_TYPE = new HashMap<>();

    static {
        for (RoomType e : values()) {
            BY_TYPE.put(e.type, e);
        }
    }

    public static RoomType valueOfType(String type){

        RoomType roomType =  BY_TYPE.get(type);
        return roomType;
    }


}
