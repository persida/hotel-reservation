package com.persidacvetkoska.hotelreservationsystemjpa.hotel.room;

import com.persidacvetkoska.hotelreservationsystemjpa.hotel.Hotel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Integer>, JpaSpecificationExecutor<Room> {

    public Optional<Room> findRoomById(Integer roomId);

    public Optional<Room> findByRoomNumberAndHotelId(String roomNumber, Integer hotelId);

    public Optional<Room> findFirstByHotel(Hotel hotel);

    public Optional<Room> findByIdAndHotelId(Integer id, Integer hotelId);

    public Page<Room> findByHotelId(Integer hotelId, Pageable pageable);

    @Query(nativeQuery = true, value = "select * from room inner join reservation on room.id = reservation.room_id where reservation.hotel_id = :hotelId and start_date <= :onDate and end_date > :onDate")
    public Page<Room> findReservedForDate(Integer hotelId, LocalDate onDate, Pageable pageable);

    @Query(nativeQuery = true, value = "select * from room where hotel_id = :hotelId and not exists (select 1 from reservation where room_id = room.id and start_date <= :onDate and end_date > :onDate and status <> 'Canceled')")
    public Page<Room> findFreeForDate(Integer hotelId, LocalDate onDate, Pageable pageable);
}
