package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import com.persidacvetkoska.hotelreservationsystemjpa.guest.Guest;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.Hotel;
import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.Room;
import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
public class Reservation {
    @Id
    @GeneratedValue
    public Integer id;
    @ManyToOne
    @JoinColumn(name = "guest_id")
    public Guest guest;
    @ManyToOne
    @JoinColumn(name = "hotel_id")
    public Hotel hotel;
    @ManyToOne
    @JoinColumn(name = "room_id")
    public Room room;
    @Column(name = "start_date")
    public LocalDate startDate;
    @Column(name = "end_date")
    public LocalDate endDate;
    @Column(name = "number_guests")
    public Integer numberGuests;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    public ReservationStatusEnum status;

    public Reservation() {
    }

    public Reservation(Guest guest, Hotel hotel, Room room, LocalDate startDate, LocalDate endDate, Integer numberGuests, ReservationStatusEnum status) {
        this.guest= guest;
        this.hotel = hotel;
        this.room = room;
        this.startDate = startDate;
        this.endDate = endDate;
        this.numberGuests = numberGuests;
        this.status = status;
    }

    public ReservationDto toDto(){
        String guestString = guest.firstName + " " + guest.lastName;
        String hotelString = hotel.name + ", " + hotel.location;
        return new ReservationDto(this.id, guestString, hotelString, room.roomNumber, startDate, endDate, numberGuests, status);
    }

}
