package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import com.persidacvetkoska.hotelreservationsystemjpa.util.ValueOfEnum;
import lombok.AllArgsConstructor;


import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@AllArgsConstructor
public class ReservationRequest {

    @NotNull(message = "guestId can't be null!")
    public Integer guestId;
    @NotNull(message = "hotelId can't be null!")
    public Integer hotelId;
    @NotNull(message = "roomId can't be null!")
    public Integer roomId;
    @NotNull(message = "startDate can't be null!")
    @Future(message = "Reservations can be made only for dates in the future!")
    public LocalDate startDate;
    @NotNull(message = "numberOfDays can't be null!")
    @Max(value = 10, message = "Reservations can be made for up to 10 days!")
    public Integer numberOfDays;
    @NotNull(message = "numberOfGuests can't be null!")
    public Integer numberOfGuests;
    @NotNull(message = "status can't be null!")
    @ValueOfEnum(enumClass = ReservationStatusEnum.class,
            anyOf = {ReservationStatusEnum.Canceled, ReservationStatusEnum.Reserved, ReservationStatusEnum.CheckedOut, ReservationStatusEnum.CheckedIn})
    public String status;
}
