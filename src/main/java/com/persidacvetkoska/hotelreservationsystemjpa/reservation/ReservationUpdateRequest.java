package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import com.persidacvetkoska.hotelreservationsystemjpa.util.ValueOfEnum;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import java.time.LocalDate;

public class ReservationUpdateRequest {
    public Integer id;
    @Future(message = "Reservations can be made only for dates in the future!")
    public LocalDate startDate;
    @Max(value = 10, message = "Reservations can be made for up to 10 days!")
    public Integer numberOfDays;
    public Integer numberOfGuests;
    @ValueOfEnum(enumClass = ReservationStatusEnum.class,
            anyOf = {ReservationStatusEnum.Canceled, ReservationStatusEnum.Reserved, ReservationStatusEnum.CheckedOut, ReservationStatusEnum.CheckedIn})
    public String status;
}
