package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import java.time.LocalDate;

@AllArgsConstructor
public class ReservationSearchRequest {

    String firstName;
    String lastName;
    String hotelName;
    String hotelLocation;
    String roomNumber;
    LocalDate startDate;

    public Specification<Reservation> generateSpecification(){
        Specification<Reservation> specification = Specification.where(null);

        if (StringUtils.hasText(firstName)){
            specification = specification.and(ReservationSpecifications.byGuestFirstNameLiteralEquals(firstName));
        }

        if (StringUtils.hasText(lastName)){
            specification = specification.and(ReservationSpecifications.byGuestLastNameLiteralEquals(lastName));
        }

        if (StringUtils.hasText(hotelName)){
            specification = specification.and(ReservationSpecifications.byHotelNameLiteralEquals(hotelName));
        }

        if (StringUtils.hasText(hotelLocation)){
            specification = specification.and(ReservationSpecifications.byHotelLocationLiteralEquals(hotelLocation));
        }

        if (StringUtils.hasText(roomNumber)){
            specification = specification.and(ReservationSpecifications.byRoomNumberLiteralEquals(roomNumber));
        }

        if (startDate != null){
            specification = specification.and(ReservationSpecifications.byStartDateLiteralEquals(startDate));
        }

        return  specification;
    }
}
