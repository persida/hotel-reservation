package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import com.persidacvetkoska.hotelreservationsystemjpa.hotel.room.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.Optional;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> , JpaSpecificationExecutor<Reservation> {

    @Query(nativeQuery = true, value = "select * from reservation where reservation.room_id = :roomId and reservation.start_date < :endDate and reservation.end_date > :startDate limit 1")
    public Optional<Reservation> findFirstReservationByRoomAndRange(Integer roomId, LocalDate startDate, LocalDate endDate);

    @Query(nativeQuery = true, value = "select * from reservation where reservation.id != :reservationId and reservation.room_id = :roomId and reservation.start_date < :endDate and reservation.end_date > :startDate limit 1")
    public Optional<Reservation> findReservationByRoomAndRangeUpdate(Integer reservationId, Integer roomId, LocalDate startDate, LocalDate endDate);

    public Optional<Reservation> findFirstByRoomId(Integer roomId);

    public Optional<Reservation> findFirstByGuestIdAndEndDateAfter(Integer guestId, LocalDate endDate);

}
