package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import org.springframework.data.jpa.domain.Specification;
import java.time.LocalDate;

public final class ReservationSpecifications {
     private ReservationSpecifications(){}

     public static Specification<Reservation> byGuestFirstNameLiteralEquals(final String firstName){
         return ((root, criteriaQuery, criteriaBuilder) ->
             criteriaBuilder.equal(criteriaBuilder.lower(root.get("guest").get("firstName")),
                     firstName.toLowerCase())
         );
     }

    public static Specification<Reservation> byGuestLastNameLiteralEquals(final String lastName){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("guest").get("lastName")),
                lastName.toLowerCase()));
    }

    public static Specification<Reservation> byHotelNameLiteralEquals(final String hotelName){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("hotel").get("name")),
                hotelName.toLowerCase()));
    }

    public static Specification<Reservation> byHotelLocationLiteralEquals(final String hotelLocation){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("hotel").get("location")),
                hotelLocation.toLowerCase()));
    }

    public static Specification<Reservation> byRoomNumberLiteralEquals(final String roomNumber){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(criteriaBuilder.lower(root.get("room").get("roomNumber")),
                roomNumber.toLowerCase()));
    }

    public static Specification<Reservation> byStartDateLiteralEquals(final LocalDate startDate){
        return ((root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("startDate"),startDate));
    }
}
