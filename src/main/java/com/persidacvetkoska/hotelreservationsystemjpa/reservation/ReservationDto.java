package com.persidacvetkoska.hotelreservationsystemjpa.reservation;

import lombok.AllArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
public class ReservationDto {
    public Integer id;
    public String guest;
    public String hotel;
    public String roomNumber;
    public LocalDate startDate;
    public LocalDate endDate;
    public Integer numberGuests;
    public ReservationStatusEnum status;
}
