package com.persidacvetkoska.hotelreservationsystemjpa.guest;

import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceNotFoundException;
import com.persidacvetkoska.hotelreservationsystemjpa.exception.ResourceValidationException;
import com.persidacvetkoska.hotelreservationsystemjpa.reservation.ReservationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class GuestService {

    private final GuestRepository repository;
    private final ReservationRepository reservationRepository;

    public GuestDto create(GuestRequest request){
        var foundGuest = repository.findFirstByFirstNameAndLastNameAndDateOfBirth(request.firstName, request.lastName, request.dateOfBirth);
        if (foundGuest.isPresent()){
            throw new ResourceValidationException("Guest already registered!");
        }
        Guest guest = request.toEntity();
        return repository.save(guest).toDto();
    }

    public GuestDto update(Integer id, GuestRequest request){
        var foundGuest = repository.findById(id)
                                .orElseThrow(() -> new ResourceNotFoundException("Guest with id = " + id + " does not exist"));
        foundGuest.firstName = request.firstName;
        foundGuest.lastName = request.lastName;
        foundGuest.dateOfBirth = request.dateOfBirth;
        foundGuest.address = request.address;
        foundGuest.city = request.city;
        foundGuest.country = request.country;
        foundGuest.email = request.email;
        foundGuest.phoneNumber = request.phoneNumber;
        return repository.save(foundGuest).toDto();
    }

    public GuestDto findById(Integer id){
       return repository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Guest with id = " + id + " not found")).toDto();
    }

    public Page<GuestDto> findPage(Pageable pageable){
        return repository.findAll(pageable).map(guest -> guest.toDto());
    }

    public void delete(Integer id){
        var foundGuest = repository.findById(id)
                                .orElseThrow(() -> new ResourceNotFoundException("Guest with id = " + id + " not found"));
        var foundReservation = reservationRepository.findFirstByGuestIdAndEndDateAfter(id, LocalDate.now().minusYears(1));
        if (foundReservation.isPresent()){
            throw new ResourceValidationException("Guest can't be deleted because there are reservations for him!");
        }
        repository.delete(foundGuest);
    }

}
