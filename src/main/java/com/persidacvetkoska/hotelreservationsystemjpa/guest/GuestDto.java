package com.persidacvetkoska.hotelreservationsystemjpa.guest;

import lombok.AllArgsConstructor;

import java.time.LocalDate;
@AllArgsConstructor
public class GuestDto {
    public Integer id;
    public String firstName;
    public String lastName;
    public LocalDate dateOfBirth;
    public String address;
    public String city;
    public String country;
    public String email;
    public String phoneNumber;

}
