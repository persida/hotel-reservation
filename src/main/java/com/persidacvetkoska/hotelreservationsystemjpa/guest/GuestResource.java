package com.persidacvetkoska.hotelreservationsystemjpa.guest;

import com.persidacvetkoska.hotelreservationsystemjpa.util.VoidResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/guests")
@RequiredArgsConstructor
public class GuestResource {

    private final GuestService service;

    @PostMapping(consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public GuestDto create(@RequestBody @Valid GuestRequest request){
        return service.create(request);
    }

    @PutMapping(path = "/{id}", consumes = MimeTypeUtils.APPLICATION_JSON_VALUE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public GuestDto update(@PathVariable Integer id, @RequestBody @Valid GuestRequest request){
        return service.update(id, request);
    }

    @GetMapping(path = "/{id}", produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public GuestDto findById(@PathVariable Integer id){
        return service.findById(id);
    }

    @GetMapping(produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
    public Page<GuestDto> findPage(Pageable pageable){
        return service.findPage(pageable);
    }

    @DeleteMapping(path = "/{id}")
    public VoidResponse delete(@PathVariable Integer id){
         service.delete(id);
         return new VoidResponse(true);
    }
}
